﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Entities;

namespace Domain.UseCase.Creditos
{
    public interface ICreditoUseCase
    {
        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <param name="cliente">The entity.</param>
        /// <returns></returns>
        Task<List<Credito>> ObtenerCreditos(); 

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<List<Credito>> ObtenerCreditosporClienteId(string id);
        /// <summary>
        /// CrearCliente
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns></returns>
        Task<Credito> CrearCredito(Credito credito);
        /// <summary>
        /// ActualizarCliente
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns></returns>
        Task<Credito> ActualizarCredito(Credito credito);

        Task<Credito> AgregarPago(Pago pago, string CreditoId);
        
        /// <summary>
        /// EliminarCliente
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<bool> EliminarCredito(string Id);
    }
}