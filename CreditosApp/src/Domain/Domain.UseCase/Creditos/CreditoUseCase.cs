﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Entities.Gateway;
using Domain.Model.Entities;
using Domain.UseCase.Common;
using credinet.comun.models.Credits;

namespace Domain.UseCase.Creditos
{
    public class CreditoUseCase : ICreditoUseCase
    {
        private readonly ICreditoRepository _creditoEntityRepository;
        private readonly IClienteRepository _clienteEntityRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ManageEventsUseCase"/> class.
        /// </summary>
        /// <param name="testEntityRepository">The test entity repository.</param>

        public CreditoUseCase(ICreditoRepository creditoEntityRepository, IClienteRepository clienteEntityRepository)
        {
            _creditoEntityRepository = creditoEntityRepository;
            _clienteEntityRepository = clienteEntityRepository;

        }



        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public async Task<List<Credito>> ObtenerCreditos()
        {
            return await _creditoEntityRepository.FindAll();
        }

        public async Task<List<Credito>> ObtenerCreditosporClienteId(string id)
        {
            return await _creditoEntityRepository.ObtenerCreditosporClienteId(id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns></returns>
        public async Task<Credito> CrearCredito(Credito credito)
        {
            Cliente cliente = await _clienteEntityRepository.ObtenerClientePorIdAsync(credito.Cliente.Id);

            credito.EstablecerCliente(cliente);

            return await _creditoEntityRepository.InsertarCreditoAsync(credito);
        }

        public async Task<Credito> ActualizarCredito(Credito credito)
        {
            return await _creditoEntityRepository.ActualizarCreditoAsync(credito);
        }

        public async Task<Credito> AgregarPago(Pago pago, string CreditoId)
        {
            return await _creditoEntityRepository.AgregarPago(pago, CreditoId);
        }
        /// <summary>
        /// EliminarCliente
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<bool> EliminarCredito(string Id)
        {
            return await _creditoEntityRepository.EliminarCreditoAsync(Id);
        }
    }
}
