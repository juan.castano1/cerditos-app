﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Entities;

namespace Domain.UseCase.Clientes
{
    public interface IClienteUseCase
    {
        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <param name="cliente">The entity.</param>
        /// <returns></returns>
        Task<List<Cliente>> ObtenerClientes();
        /// <summary>
        /// CrearCliente
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns></returns>
        Task<Cliente> CrearCliente(Cliente cliente);
        /// <summary>
        /// ActualizarCliente
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns></returns>
        Task<Cliente>  ActualizarCliente(Cliente cliente);
        /// <summary>
        /// EliminarCliente
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<bool> EliminarCliente(string Id);
    }
}
