﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Entities;
using Domain.Model.Entities.Gateway;

using Domain.UseCase.Common;
using Microsoft.Extensions.Logging;

namespace Domain.UseCase.Clientes
{

    public class ClienteUseCase : IClienteUseCase
    {
        private readonly IClienteRepository _clienteEntityRepository;


        /// <summary>
        /// Initializes a new instance of the <see cref="ManageEventsUseCase"/> class.
        /// </summary>
        /// <param name="testEntityRepository">The test entity repository.</param>

        public ClienteUseCase(IClienteRepository clienteEntityRepository)
        {
            _clienteEntityRepository = clienteEntityRepository;

        }



        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public async Task<List<Cliente>> ObtenerClientes()
        {
            return await _clienteEntityRepository.FindAll();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns></returns>
        public async Task<Cliente> CrearCliente(Cliente cliente)
        {

            return await _clienteEntityRepository.InsertarClienteAsync(cliente);
        }

        public async Task<Cliente> ActualizarCliente(Cliente cliente)
        {
            return await _clienteEntityRepository.ActualizarClienteAsync(cliente);
        }
        /// <summary>
        /// EliminarCliente
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<bool> EliminarCliente(string Id)
        {
            return await _clienteEntityRepository.EliminarClienteAsync(Id);
        }
    }
}
