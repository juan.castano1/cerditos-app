﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Entities.Gateway
{
    public interface ICreditoRepository
    {
        /// <summary>
        /// FindAll
        /// </summary>
        /// <returns>Cliente list</returns>
        /// <summary>
        Task<List<Credito>> FindAll();


        Task<List<Credito>> ObtenerCreditosporClienteId(string clienteId);
        /// 
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns></returns>
        Task<Credito> InsertarCreditoAsync(Credito credito);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns></returns>
        Task<Credito> ActualizarCreditoAsync(Credito credito);


        Task<Credito> AgregarPago(Pago pago, string CreditoId);
        /// <summary>
        /// EliminarCliente
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<bool> EliminarCreditoAsync(string Id);
    }
}