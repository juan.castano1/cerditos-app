﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Entities.Gateway
{
    public interface IClienteRepository
    {
        /// <summary>
        /// FindAll
        /// </summary>
        /// <returns>Cliente list</returns>
        /// <summary>
        Task<List<Cliente>> FindAll();

        /// 
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns></returns>
        Task<Cliente> InsertarClienteAsync(Cliente cliente);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns></returns>
        Task<Cliente> ActualizarClienteAsync(Cliente cliente);
        /// <summary>
        /// EliminarCliente
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<bool> EliminarClienteAsync(string Id);


        Task<Cliente> ObtenerClientePorIdAsync(string Id);
    }
}