﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Entities
{
    public class Credito
    {
        private object value;

        public Credito(string id, Cliente cliente, string estado, decimal monto, decimal tasaInteres, int plazoMeses, List<Pago> pagosRealizados)
        {
            Id = id;
            Cliente = cliente;
            Estado = estado;
            Monto = monto;
            TasaInteres = tasaInteres;
            PlazoMeses = plazoMeses;
            PagosRealizados = pagosRealizados;
        }
        public Credito(string clienteId, string estado, decimal monto, decimal tasaInteres, int plazoMeses, List<Pago> pagosRealizados)
        {
            Cliente = new Cliente(clienteId);
            Estado = estado;
            Monto = monto;
            TasaInteres = tasaInteres;
            PlazoMeses = plazoMeses;
            PagosRealizados = pagosRealizados;
        }
        public Credito(Cliente cliente, string estado, decimal monto, decimal tasaInteres, int plazoMeses, List<Pago> pagosRealizados)
        {
            Cliente = cliente;
            Estado = estado;
            Monto = monto;
            TasaInteres = tasaInteres;
            PlazoMeses = plazoMeses;
            PagosRealizados = pagosRealizados;
        }

        public Credito(Cliente cliente, string estado, decimal monto, decimal tasaInteres, int plazoMeses, object value)
        {
            Cliente = cliente;
            Estado = estado;
            Monto = monto;
            TasaInteres = tasaInteres;
            PlazoMeses = plazoMeses;
            this.value = value;
        }

        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; private set; }

        public Cliente Cliente { get; private set; }

        /// <summary>
        /// Nombre
        /// </summary>
        public string Estado { get; private set; }

        public decimal Monto { get; set; }

        public decimal TasaInteres { get; set; }
        public int PlazoMeses { get; set; }

        public List<Pago>? PagosRealizados { get; set; }

        public void EstablecerCliente(Cliente cliente) => Cliente = cliente;

    }
}

