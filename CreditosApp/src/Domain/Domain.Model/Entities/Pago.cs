﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Entities
{
    public class Pago
    {

      /*  /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="numeroCuota"></param>
        /// <param name="fechaPago"></param>
        /// <param name="montoPago"></param>
        public Pago(string id , int numeroCuota, DateTime fechaPago, decimal montoPago)
        {
            Id = id;
            NumeroCuota = numeroCuota;
            FechaPago = fechaPago;
            MontoPago = montoPago;
        }*/
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="numeroCuota"></param>
        /// <param name="fechaPago"></param>
        /// <param name="montoPago"></param>
        public Pago(int numeroCuota, DateTime fechaPago, decimal montoPago)
        {
            NumeroCuota = numeroCuota;
            FechaPago = fechaPago;
            MontoPago = montoPago;
        }
        /// <summary>
        /// Id
        /// </summary>
       // public string Id { get; private set; }
        /// <summary>
        /// NumeroCuota
        /// </summary>
        public int NumeroCuota { get; set; }
        /// <summary>
        /// DateTime
        /// </summary>
        public DateTime FechaPago { get; set; }
        /// <summary>
        /// MontoPago
        /// </summary>
        public decimal MontoPago { get; set; }
    }
}
