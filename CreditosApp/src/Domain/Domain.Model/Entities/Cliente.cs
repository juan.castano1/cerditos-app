﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using credinet.comun.models.Credits;

namespace Domain.Model.Entities
{
    public class Cliente
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; private set; }

        /// <summary>
        /// Valor Nombre
        /// </summary>
        public string Nombre { get; private set; }


        /// <summary>
        /// Dirección
        /// </summary>
        public string Direccion { get; private set; }

        /// <summary>
        /// Valor Numero del Celular
        /// </summary>
        public string Celular { get; private set; }

        /// <summary>
        /// Valor de el Correo
        /// </summary>
        public string Correo { get; private set; }


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="nombre"></param>
        /// <param name="direccion"></param>
        /// <param name="celular"></param>
        /// <param name="correo"></param>

        public Cliente(string id, string nombre, string direccion,  string celular,
            string correo)
        {
            Id = id;
            Nombre = nombre;
            Direccion = direccion;
            Celular = celular;
            Correo = correo;
       
        }
        public Cliente(string id)
        {
            Id = id;
        }

        public Cliente(string nombre, string direccion, string celular, string correo)
        {
            Nombre = nombre;
            Direccion = direccion;
            Celular = celular;
            Correo = correo;
        }

    }
}
