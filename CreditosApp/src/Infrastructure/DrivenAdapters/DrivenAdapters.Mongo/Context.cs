using Domain.Model.Entities;
using DrivenAdapters.Mongo.Entities;
using MongoDB.Driver;
using System.Diagnostics.CodeAnalysis;

namespace DrivenAdapters.Mongo
{
    /// <summary>
    /// Context is an implementation of <see cref="IContext"/>
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class Context : IContext
    {
        private readonly IMongoDatabase _database;

        /// <summary>
        /// crea una nueva instancia de la clase <see cref="Context"/>
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="databaseName"></param>
        public Context(string connectionString, string databaseName)
        {
            MongoClient _mongoClient = new MongoClient(connectionString);
            _database = _mongoClient.GetDatabase(databaseName);
        }

        /// <summary>
        /// Tipo Contrato Entity
        /// </summary>
        public IMongoCollection<ClienteEntity> Cliente => _database.GetCollection<ClienteEntity>("Cliente");

        public IMongoCollection<CreditoEntity> Credito => _database.GetCollection<CreditoEntity>("Credito");

        public IMongoCollection<PagoEntity> Pago => _database.GetCollection<PagoEntity>("Pago");

    }
}