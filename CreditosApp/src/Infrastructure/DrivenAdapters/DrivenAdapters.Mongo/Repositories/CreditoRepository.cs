﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using credinet.comun.models.Credits;
using Domain.Model.Entities;
using Domain.Model.Entities.Gateway;
using DrivenAdapters.Mongo.Entities;
using MongoDB.Driver;


namespace DrivenAdapters.Mongo.Repositories
{
    public class CreditoRepository : ICreditoRepository
    {
        private readonly IMongoCollection<CreditoEntity> _collectionCredito;

        public CreditoRepository(IContext mongodb)
        {
            _collectionCredito = mongodb.Credito;
        }

        public async Task<List<Credito>> FindAll()
        {
            IAsyncCursor<CreditoEntity> creditosEntity =
                await _collectionCredito.FindAsync(Builders<CreditoEntity>.Filter.Empty);

            List<Credito> creditos = creditosEntity.ToEnumerable()
                .Select(CreditosEntity => CreditosEntity.AsEntity()).ToList();

            return creditos;

        }
        public async Task<List<Credito>> ObtenerCreditosporClienteId(string clienteId)
        {
            FilterDefinition<CreditoEntity> filterCredito =
                Builders<CreditoEntity>.Filter.Eq(credito => credito.Cliente.Id, clienteId);

            IAsyncCursor<CreditoEntity> creditosEntity =
           await _collectionCredito.FindAsync(filterCredito);

            List<Credito> creditos = creditosEntity.ToEnumerable()
                .Select(CreditosEntity => CreditosEntity.AsEntity()).ToList();

            return creditos;

        }
        
        public async Task<Credito> InsertarCreditoAsync(Credito crearCredito)
        {
            CreditoEntity nuevoCredito =
                new( new ClienteEntity( crearCredito.Cliente.Id,
                crearCredito.Cliente.Nombre,
                crearCredito.Cliente.Direccion,
                crearCredito.Cliente.Celular,
                crearCredito.Cliente.Correo
                ),
                crearCredito.Estado,
                crearCredito.Monto, 
                crearCredito.TasaInteres, 
                crearCredito.PlazoMeses, 
                crearCredito.PagosRealizados.Select(pago =>
            new PagoEntity( pago.NumeroCuota, pago.FechaPago, pago.MontoPago)
                ).ToList());

            await _collectionCredito.InsertOneAsync(nuevoCredito);

            return nuevoCredito.AsEntity();

        }
        /// <param name="monto"></param>
        /// <param name="tasaInteres"></param>
        /// <param name="plazoMeses"></param>
        /// <param name="pagosRealizados"></param>
        public async Task<Credito> ActualizarCreditoAsync(Credito credito)
        {
            UpdateDefinition<CreditoEntity> cambiosCredito = Builders<CreditoEntity>
            .Update 
            .Set(c => c.Estado, credito.Estado)
            .Set(c => c.PlazoMeses, credito.PlazoMeses)
            .Set(c => c.Monto, credito.Monto)
            .Set(c => c.PlazoMeses, credito.PlazoMeses)
            .Set(c => c.TasaInteres, credito.TasaInteres)
            .Set(c => c.PagosRealizados, credito.PagosRealizados.Select(pago => 
            new PagoEntity( pago.NumeroCuota, pago.FechaPago, pago.MontoPago)
                ).ToList());

            UpdateResult result = await _collectionCredito.
                UpdateOneAsync(Builders<CreditoEntity>
                .Filter.Eq(c => c.Id, credito.Id), cambiosCredito);
            return credito;
        }



        public async  Task<Credito> AgregarPago(Pago pago, string CreditoId)
        {
            FilterDefinition<CreditoEntity> filterCredito =
                Builders<CreditoEntity>.Filter.Eq(credito => credito.Id, CreditoId);

            CreditoEntity Entity =
                await _collectionCredito.Find(filterCredito).FirstOrDefaultAsync();
            Entity.PagosRealizados.Add(new PagoEntity(
                  pago.NumeroCuota, pago.FechaPago, pago.MontoPago));

            UpdateDefinition<CreditoEntity> cambiosCredito = Builders<CreditoEntity>
            .Update
            .Set(c => c.PagosRealizados, Entity.PagosRealizados.Select(pago =>
            new PagoEntity(pago.NumeroCuota, pago.FechaPago, pago.MontoPago)
                ).ToList());

            UpdateResult result = await _collectionCredito.
                UpdateOneAsync(Builders<CreditoEntity>
                .Filter.Eq(c => c.Id, CreditoId), cambiosCredito);
            return Entity.AsEntity();


        }
        /// <summary>
        /// EliminarCliente
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<bool> EliminarCreditoAsync(string Id)
        {
            return _collectionCredito.DeleteOneAsync(c => c.Id == Id).Result.IsAcknowledged;
        }
    }

}