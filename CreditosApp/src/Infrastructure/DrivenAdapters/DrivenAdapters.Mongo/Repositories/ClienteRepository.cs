﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Entities;
using Domain.Model.Entities.Gateway;
using DrivenAdapters.Mongo.Entities;
using Microsoft.Azure.ServiceBus;
using MongoDB.Driver;

namespace DrivenAdapters.Mongo.Repositories
{
    public class ClienteRepository : IClienteRepository
    {
        private readonly IMongoCollection<ClienteEntity> _collectionCliente;

        public ClienteRepository(IContext mongodb)
        {
            _collectionCliente = mongodb.Cliente;
        }

        public async Task<List<Cliente>> FindAll()
        {
            IAsyncCursor<ClienteEntity> clientesEntity =
                await _collectionCliente.FindAsync(Builders<ClienteEntity>.Filter.Empty);

            List<Cliente> clientes = clientesEntity.ToEnumerable()
                .Select(clientesEntity => clientesEntity.AsEntity()).ToList();

            return clientes;

        }

        public async Task<Cliente> ObtenerClientePorIdAsync(string Id)
        {
            FilterDefinition<ClienteEntity> filterCliente =
                Builders<ClienteEntity>.Filter.Eq(cliente => cliente.Id, Id);

            ClienteEntity tipoEntity =
                await _collectionCliente.Find(filterCliente).FirstOrDefaultAsync();

            return tipoEntity?.AsEntity();

        }
        public async Task<Cliente> InsertarClienteAsync(Cliente crearCliente)
        {
            ClienteEntity nuevoCliente =
                new(crearCliente.Nombre, crearCliente.Direccion, crearCliente.Celular, crearCliente.Correo);

            await _collectionCliente.InsertOneAsync(nuevoCliente);

            return nuevoCliente.AsEntity();

        }

        public async Task<Cliente> ActualizarClienteAsync(Cliente cliente)
        {
            UpdateDefinition<ClienteEntity> cambiosCliente = Builders<ClienteEntity>
            .Update
            .Set(c => c.Nombre, cliente.Nombre)
            .Set(c => c.Direccion, cliente.Direccion)
            .Set(c => c.Celular, cliente.Celular)
            .Set(c => c.Correo, cliente.Correo);

            UpdateResult result = await _collectionCliente.
                UpdateOneAsync(Builders<ClienteEntity>
                .Filter.Eq(c => c.Id, cliente.Id), cambiosCliente);
            return cliente;
        }
        /// <summary>
        /// EliminarCliente
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<bool> EliminarClienteAsync(string Id)
        {
            return  _collectionCliente.DeleteOneAsync(c => c.Id == Id).Result.IsAcknowledged;
        }
    }

}