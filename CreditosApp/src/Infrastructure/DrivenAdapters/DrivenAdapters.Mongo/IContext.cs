﻿using Domain.Model.Entities;
using DrivenAdapters.Mongo.Entities;
using MongoDB.Driver;

namespace DrivenAdapters.Mongo
{
    /// <summary>
    /// Interfaz Mongo context contract.
    /// </summary>
    public interface IContext
    {
        /// <summary>
        /// Colección de Cliente
        /// </summary>
        public IMongoCollection<ClienteEntity> Cliente { get; }

        /// <summary>
        /// Colección de Crédito
        /// </summary>
        public IMongoCollection<CreditoEntity> Credito { get; }

        /// <summary>
        /// Colección de pago
        /// </summary>
        public IMongoCollection<PagoEntity> Pago { get; }

    }
}