﻿using Domain.Model.Entities;
using DrivenAdapters.Mongo.Entities.Base;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DrivenAdapters.Mongo.Entities
{
    public class ClienteEntity : EntityBase, IDomainEntity<Cliente>
    {


        /// <summary>
        /// Valor Nombre
        /// </summary>
        [BsonElement(elementName: "nombre")]
        public string Nombre { get; private set; }

        /// <summary>
        /// Dirección
        /// </summary>
        [BsonElement(elementName: "direccion")]
        public string Direccion { get; private set; }

        /// <summary>
        /// Valor Numero del Celular
        /// </summary>
        [BsonElement(elementName: "celular")]
        public string Celular { get; private set; }

        /// <summary>
        /// Valor de el Correo
        /// </summary>
        [BsonElement(elementName: "correo")]
        public string Correo { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="direccion"></param>
        /// <param name="celular"></param>
        /// <param name="correo"></param>
        /// <param name="creditoId"></param>
        public Cliente AsEntity() => new(
            Id,
            Nombre,
            Direccion,
            Celular, 
            Correo);

        public ClienteEntity( string nombre, string direccion, string celular,
            string correo)
        {

            Nombre = nombre;

            Direccion = direccion;
            Celular = celular;
            Correo = correo;

        }
        public ClienteEntity(string id,string nombre, string direccion, string celular,
    string correo)
        {
            Id = id;
            Nombre = nombre;

            Direccion = direccion;
            Celular = celular;
            Correo = correo;

        }
    }
}
