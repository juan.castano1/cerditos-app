﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Entities;
using DrivenAdapters.Mongo.Entities.Base;
using MongoDB.Bson.Serialization.Attributes;


namespace DrivenAdapters.Mongo.Entities
{
    public class CreditoEntity : EntityBase, IDomainEntity<Credito>
    {
        /// <summary>
        /// constructor completo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="estado"></param>
        /// <param name="monto"></param>
        /// <param name="tasaInteres"></param>
        /// <param name="plazoMeses"></param>
        /// <param name="pagosRealizados"></param>
        public CreditoEntity(string id, string estado, decimal monto, decimal tasaInteres, int plazoMeses, List<PagoEntity> pagosRealizados)
        {
            Id = id;
            Estado = estado;
            Monto = monto;
            TasaInteres = tasaInteres;
            PlazoMeses = plazoMeses;
            PagosRealizados = pagosRealizados;
        }
        /// <summary>
        /// Constructor sin Id
        /// </summary>
        /// <param name="estado"></param>
        /// <param name="monto"></param>
        /// <param name="tasaInteres"></param>
        /// <param name="plazoMeses"></param>
        /// <param name="pagosRealizados"></param>
        public CreditoEntity(string estado, decimal monto, decimal tasaInteres, int plazoMeses, List<PagoEntity> pagosRealizados)
        {

            Estado = estado;
            Monto = monto;
            TasaInteres = tasaInteres;
            PlazoMeses = plazoMeses;
            PagosRealizados = pagosRealizados;
        }

        public CreditoEntity(ClienteEntity cliente, string estado, decimal monto, decimal tasaInteres, int plazoMeses, List<PagoEntity> pagosRealizados)
        {
            Cliente = cliente;
            Estado = estado;
            Monto = monto;
            TasaInteres = tasaInteres;
            PlazoMeses = plazoMeses;
            PagosRealizados = pagosRealizados;
        }
        /// <summary>
        /// Cliente
        /// </summary>
        [BsonElement(elementName: "cliente")]
        public ClienteEntity Cliente { get; private set; }
        /// <summary>
        /// Estado
        /// </summary>

        [BsonElement(elementName: "estado")]
        public string Estado { get; private set; }

        /// <summary>
        /// Monto
        /// </summary>
        [BsonElement(elementName: "monto")]
        public decimal Monto { get; private set; }

        /// <summary>
        /// TasaInteres
        /// </summary>
        [BsonElement(elementName: "tasaInteres")]
        public decimal TasaInteres { get; private set; }

        /// <summary>
        /// PlazoMeses
        /// </summary>
        [BsonElement(elementName: "plazoMeses")]
        public int PlazoMeses { get; private set; }

        /// <summary>
        /// PagosRealizados
        /// </summary>
        [BsonElement(elementName: "pagosRealizados")]
        public List<PagoEntity> PagosRealizados { get; private set; }

        /// <summary>
        /// transforma CreditoEntity a Credito
        /// </summary>
        /// <returns>Credito</returns>
        public Credito AsEntity() => new(Id, Cliente.AsEntity(), Estado, Monto, TasaInteres,
                PlazoMeses, PagosRealizados?.Select(Entity => Entity.AsEntity()).ToList()
                );

        /// <summary>
        /// transforma la lista PagoEntity a lista Pago
        /// </summary>
        /// <returns>List<Pago></returns>
      /*  public List<Pago> PagosRealizadosListAsEntity()
        {
            List<Pago> pagosEntity = new();
            PagosRealizados.ForEach(p => { pagosEntity.Add(p.AsEntity()); }
                );
            return pagosEntity;
        }*/
    }
}
