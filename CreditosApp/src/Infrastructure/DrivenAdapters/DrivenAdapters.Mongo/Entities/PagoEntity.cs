﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Entities;
using DrivenAdapters.Mongo.Entities.Base;
using MongoDB.Bson.Serialization.Attributes;

namespace DrivenAdapters.Mongo.Entities
{
    public class PagoEntity 
    {  

        /// <summary>
        /// NumeroCuota
        /// </summary>
        [BsonElement(elementName: "numeroCuota")]
        public int NumeroCuota { get; private set; }

        /// <summary>
        /// FechaPago
        /// </summary>
        [BsonElement(elementName: "fechaPago")]
        public DateTime FechaPago { get; private set; }

        /// <summary>
        /// MontoPago        
        /// </summary>
        [BsonElement(elementName: "montoPago")]
        public decimal MontoPago { get; private set; }

        /// <summary>
        /// Pago como entidad
        /// </summary>
        public Pago AsEntity() => new( NumeroCuota, FechaPago, MontoPago);

        /// <summary>
        /// Constructor PagoEntity
        /// </summary>
        public PagoEntity( int numeroCuota, DateTime fechaPago, decimal montoPago)
        {

            NumeroCuota = numeroCuota;
            FechaPago = fechaPago;
            MontoPago = montoPago;
        }
    }
}
