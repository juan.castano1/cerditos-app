﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Entities;

namespace EntryPoints.ReactiveWeb.Entity
{
    public class PagoRequest
    {
        public string CreditoId { get; set; }
        public int NumeroCuota { get;  set; }

        /// <summary>
        /// FechaPago
        /// </summary>
   
        public DateTime FechaPago { get;  set; }

        /// <summary>
        /// MontoPago        
        /// </summary>

        public decimal MontoPago { get;  set; }

        /// <summary>
        /// Pago como entidad
        /// </summary>
        public Pago AsEntity() => new( NumeroCuota, FechaPago, MontoPago);
    }
}
