﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Entities;

namespace EntryPoints.ReactiveWeb.Entity
{
    public class IdStringRequest
    {
        public string Id { get; set; }

    }
}