﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Entities;


namespace EntryPoints.ReactiveWeb.Entity
{
    public class CreditoRequest
    {
      
        public string ClienteId { get;  set; }
        /// <summary>
        /// Estado
        /// </summary>

     
        public string Estado { get;  set; }

        /// <summary>
        /// Monto
        /// </summary>
      
        public decimal Monto { get;  set; }

        /// <summary>
        /// TasaInteres
        /// </summary>
  
        public decimal TasaInteres { get;  set; }

        /// <summary>
        /// PlazoMeses
        /// </summary>
      
        public int PlazoMeses { get;  set; }

        /// <summary>
        /// PagosRealizados
        /// </summary>

      //  public List<Pago>? PagosRealizados { get;  set; }

        /// <summary>
        /// transforma CreditoEntity a Credito
        /// </summary>
        /// <returns>Credito</returns>
        public Credito AsEntity() => new(

            ClienteId,
            Estado,
            Monto,
            TasaInteres,
            PlazoMeses,  new List<Pago>() 
           // , PagosRealizadosListAsEntity()
            );

        /// <summary>
        /// transforma la lista PagoEntity a lista Pago
        /// </summary>
        /// <returns>List<Pago></returns>
    /*   public List<Pago> PagosRealizadosListAsEntity()
        {
            List<Pago> pagosEntity = new();
            PagosRealizados.ForEach(p => { pagosEntity.Add(p.AsEntity()); }
                );
            return pagosEntity;
        }*/
    }
}
