﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Entities;

namespace EntryPoints.ReactiveWeb.Entity
{
 
 public class ClienteRequest
    {

        /// <summary>
        /// Valor Nombre
        /// </summary>
        public string Nombre { get;  set; }


        /// <summary>
        /// Dirección
        /// </summary>
        public string Direccion { get;  set; }

        /// <summary>
        /// Valor Numero del Celular
        /// </summary>
        public string Celular { get;  set; }

        /// <summary>
        /// Valor de el Correo
        /// </summary>
        public string Correo { get;  set; }


        public Cliente AsEntity() =>
            new(Nombre, Direccion, Celular, Correo);



    }
}
