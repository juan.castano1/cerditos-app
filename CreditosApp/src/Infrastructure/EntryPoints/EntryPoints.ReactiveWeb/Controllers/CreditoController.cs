﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Entities;
using Domain.UseCase.Clientes;
using Domain.UseCase.Common;
using Domain.UseCase.Creditos;
using EntryPoints.ReactiveWeb.Base;
using EntryPoints.ReactiveWeb.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EntryPoints.ReactiveWeb.Controllers
{

 /// <summary>
 /// EntityController
 /// </summary>
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/[controller]/[action]")]
    public class CreditoController : AppControllerBase<CreditoController>
    {
        private readonly ICreditoUseCase _creditoUseCase;


        /// <summary>
        /// CreditoController
        /// </summary>
        /// <param name="creditoUseCase"></param>
        /// <param name="eventsService"></param>
        public CreditoController(ICreditoUseCase creditoUseCase,
            IManageEventsUseCase eventsService) : base(eventsService)
        {

            _creditoUseCase = creditoUseCase;
        }

        /// <summary>
        /// Método para Obtener los Clientes
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> ObtenerCreditos() =>
            await HandleRequestAsync(
                async () =>
                {
                    return await _creditoUseCase.ObtenerCreditos();
                }, "");


        [HttpGet()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> ObtenerCreditosporClienteId([FromQuery] string id) =>
    await HandleRequestAsync(
        async () =>
        {
            return await _creditoUseCase.ObtenerCreditosporClienteId(id);
        }, "");
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CreditoRequest"></param>
        /// <returns></returns>
        [HttpPost()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> CrearCredito(CreditoRequest CreditoRequest) =>
    await HandleRequestAsync(
        async () =>
        {
            Credito nuevoCredito = CreditoRequest.AsEntity();
            return await _creditoUseCase.CrearCredito(nuevoCredito);
        }, "");

        /// <summary>
        /// 
        /// </summary>
        /// <param name="credito"></param>
        /// <returns></returns>
        [HttpPut()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> ActualizarCredito(Credito credito) =>
    await HandleRequestAsync(
        async () =>
        {

            return await _creditoUseCase.ActualizarCredito(credito);
        }, "");




        [HttpPut()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> AgregarPago(PagoRequest pagoRequest) =>
await HandleRequestAsync(
async () =>
{
    Pago nuevopago = pagoRequest.AsEntity();
    return await _creditoUseCase.AgregarPago(nuevopago, pagoRequest.CreditoId);
}, "");











        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> EliminarCredito([FromBody] string id) =>
    await HandleRequestAsync(
        async () =>
        {
            return await _creditoUseCase.EliminarCredito(id);
        }, "");




    }
}
