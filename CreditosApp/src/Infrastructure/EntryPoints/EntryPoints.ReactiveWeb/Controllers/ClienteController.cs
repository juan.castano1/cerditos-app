﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Model.Entities;
using Domain.UseCase.Clientes;
using Domain.UseCase.Common;
using EntryPoints.ReactiveWeb.Base;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

using static credinet.comun.negocio.RespuestaNegocio<credinet.exception.middleware.models.ResponseEntity>;
using static credinet.exception.middleware.models.ResponseEntity;

using EntryPoints.ReactiveWeb.Entity;



namespace EntryPoints.ReactiveWeb.Controllers
{

    /// <summary>
    /// EntityController
    /// </summary>
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/[controller]/[action]")]
    public class ClienteController : AppControllerBase<ClienteController>
    {
        private readonly IClienteUseCase _clienteUseCase;


        /// <summary>
        /// Initializes a new instance of the <see cref="ClienteController"/> class.
        /// </summary>
        /// <param name="testNegocio">The test negocio.</param>
        /// <param name="logger">The logger.</param>
        public ClienteController(IClienteUseCase clienteUseCase,
            IManageEventsUseCase eventsService) : base(eventsService)
        {

            _clienteUseCase = clienteUseCase;
        }

        /// <summary>
        /// Método para Obtener los Clientes
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> ObtenerClientes() => 
            await HandleRequestAsync(
                async () =>
                {
                    return await _clienteUseCase.ObtenerClientes();
                },"");

        [HttpPost()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> CrearCliente(ClienteRequest clienteRequest) =>
    await HandleRequestAsync(
        async () =>
        {
            Cliente nuevoCliente = clienteRequest.AsEntity();
            return await _clienteUseCase.CrearCliente(nuevoCliente);
        }, "");
        [HttpPut()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> ActualizarCliente(Cliente cliente) =>
    await HandleRequestAsync(
        async () =>
        {

            return await _clienteUseCase.ActualizarCliente(cliente);
        }, "");
        [HttpDelete()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> EliminarCliente([FromBody] string id) =>
    await HandleRequestAsync(
        async () =>
        { 
            return await _clienteUseCase.EliminarCliente(id);
        }, "");




    }
}