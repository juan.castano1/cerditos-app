﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Entities;
using Domain.UseCase.Clientes;
using Domain.UseCase.Common;
using Domain.UseCase.Creditos;
using EntryPoints.ReactiveWeb.Base;
using EntryPoints.ReactiveWeb.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EntryPoints.ReactiveWeb.Controllers
{

 /// <summary>
 /// EntityController
 /// </summary>
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/[controller]/[action]")]
    public class PagoController : AppControllerBase<PagoController>
    {
      private readonly ICreditoUseCase _creditoUseCase;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="clienteUseCase"></param>
        /// <param name="eventsService"></param>
        public PagoController(ICreditoUseCase clienteUseCase,
            IManageEventsUseCase eventsService) : base(eventsService)
        {

            _creditoUseCase = clienteUseCase;
        }/*

        /// <summary>
        /// Método para Obtener los Clientes
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> ObtenerPagos() =>
            await HandleRequestAsync(
                async () =>
                {
                    return await _clienteUseCase.ObtenerClientes();
                }, "");

        [HttpPost()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> AgregarPago(PagoRequest pagoRequest) =>
    await HandleRequestAsync(
        async () =>
        {
            Pago nuevopago = pagoRequest.AsEntity();
            return await _creditoUseCase.AgregarPago(nuevopago, pagoRequest.CreditoId);
        }, "");
        [HttpPut()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> ActualizarPago(Cliente cliente) =>
    await HandleRequestAsync(
        async () =>
        {

            return await _clienteUseCase.ActualizarCliente(cliente);
        }, "");
        [HttpDelete()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> EliminarPago([FromBody] string id) =>
    await HandleRequestAsync(
        async () =>
        {
            return await _clienteUseCase.EliminarCliente(id);
        }, "");


        */

    }
}